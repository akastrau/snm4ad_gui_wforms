
#pragma once
#include "remoteCommandsDialog.h"

namespace SNM4ADGUIWFORMS {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for remoteCommandsDialog
	/// </summary>
	public ref class remoteCommandsDialog : public System::Windows::Forms::Form
	{
	public:
		remoteCommandsDialog(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

		System::Int16^ GetrpcCall();
		

	private:
		System::Int16 ^rpcCall;
	

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~remoteCommandsDialog()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected:
	private: System::Windows::Forms::Button^  aboutMeButton;
	private: System::Windows::Forms::Button^  rebootButton;
	private: System::Windows::Forms::Button^  shutdownButton;
	private: System::Windows::Forms::Button^  closeButton;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(remoteCommandsDialog::typeid));
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->aboutMeButton = (gcnew System::Windows::Forms::Button());
			this->rebootButton = (gcnew System::Windows::Forms::Button());
			this->shutdownButton = (gcnew System::Windows::Forms::Button());
			this->closeButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->aboutMeButton);
			this->groupBox1->Controls->Add(this->rebootButton);
			this->groupBox1->Controls->Add(this->shutdownButton);
			this->groupBox1->Location = System::Drawing::Point(25, 22);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(226, 174);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Wybierz zdalne polecenie:";
			// 
			// aboutMeButton
			// 
			this->aboutMeButton->Location = System::Drawing::Point(27, 115);
			this->aboutMeButton->Name = L"aboutMeButton";
			this->aboutMeButton->Size = System::Drawing::Size(163, 23);
			this->aboutMeButton->TabIndex = 2;
			this->aboutMeButton->Text = L"Wy�wietl stron� \"o mnie\"";
			this->aboutMeButton->UseVisualStyleBackColor = true;
			this->aboutMeButton->Click += gcnew System::EventHandler(this, &remoteCommandsDialog::aboutMeButton_Click);
			// 
			// rebootButton
			// 
			this->rebootButton->Location = System::Drawing::Point(28, 73);
			this->rebootButton->Name = L"rebootButton";
			this->rebootButton->Size = System::Drawing::Size(162, 23);
			this->rebootButton->TabIndex = 1;
			this->rebootButton->Text = L"Zrestartuj wszystkie komputery";
			this->rebootButton->UseVisualStyleBackColor = true;
			this->rebootButton->Click += gcnew System::EventHandler(this, &remoteCommandsDialog::rebootButton_Click);
			// 
			// shutdownButton
			// 
			this->shutdownButton->Location = System::Drawing::Point(27, 33);
			this->shutdownButton->Name = L"shutdownButton";
			this->shutdownButton->Size = System::Drawing::Size(163, 23);
			this->shutdownButton->TabIndex = 0;
			this->shutdownButton->Text = L"Wy��cz wszystkie komputery";
			this->shutdownButton->UseVisualStyleBackColor = true;
			this->shutdownButton->Click += gcnew System::EventHandler(this, &remoteCommandsDialog::shutdownButton_Click);
			// 
			// closeButton
			// 
			this->closeButton->Location = System::Drawing::Point(94, 212);
			this->closeButton->Name = L"closeButton";
			this->closeButton->Size = System::Drawing::Size(75, 23);
			this->closeButton->TabIndex = 3;
			this->closeButton->Text = L"Zamknij";
			this->closeButton->UseVisualStyleBackColor = true;
			this->closeButton->Click += gcnew System::EventHandler(this, &remoteCommandsDialog::closeButton_Click);
			// 
			// remoteCommandsDialog
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->closeButton);
			this->Controls->Add(this->groupBox1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"remoteCommandsDialog";
			this->Text = L"Wy�lij zdalne polecenie";
			this->groupBox1->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void closeButton_Click(System::Object^  sender, System::EventArgs^  e) {

		this->rpcCall = (System::Int16)666;
		Close();
		
	}
private: System::Void shutdownButton_Click(System::Object^  sender, System::EventArgs^  e) {
	
	this->rpcCall = (System::Int16)1;
	Close();
	
}
private: System::Void rebootButton_Click(System::Object^  sender, System::EventArgs^  e) {

	this->rpcCall = (System::Int16)2;
	Close();
}
private: System::Void aboutMeButton_Click(System::Object^  sender, System::EventArgs^  e) {

	this->rpcCall = (System::Int16)3;
	Close();
}
};
}
