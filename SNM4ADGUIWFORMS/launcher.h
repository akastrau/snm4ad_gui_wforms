#pragma once

namespace SNM4ADGUIWFORMS {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Net;
	using namespace System::Security;
	using namespace System::DirectoryServices;
	using namespace Threading;


	/// <summary>
	/// Summary for launcher
	/// </summary>
	public ref class launcher : public System::Windows::Forms::Form
	{
	public:
		launcher(void)
		{
			InitializeComponent();
			this->ErrorImage->Image = SystemIcons::Error->ToBitmap();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~launcher()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  Launcher_label_welcome;
	private: System::Windows::Forms::Label^  statusLabel1;
	private: System::Windows::Forms::Label^  statusLabel2;
	private: System::Windows::Forms::Timer^  launcher_delay;
	private: System::Windows::Forms::PictureBox^  ErrorImage;
	private: System::Windows::Forms::Label^  errorLabel;
	private: System::Windows::Forms::Button^  OnErrorButton;
	private: System::Windows::Forms::Timer^  refreshView;

	private: System::Windows::Forms::PictureBox^  progress_status;


	protected:

	public:
		IPAddress^ GetHostIPAddress();
		String^ GetHostName();
		NetworkInformation::UnicastIPAddressInformation^ GetUnicastAddress();
		IPAddress^ GetSendAddress();
		String^ GetDomainName();
		String^ GetUserNameA();
		System::Byte GetMode();
		Int16 GetSendPort();
		Int16 GetListenPort();

	private:
		System::Void GetNetworkInformation();
		System::Void GetUserInformation();
		System::Void ChangeStatusText(String ^StatusText);
		System::Void ShowErrorMessage(String ^ErrorMessage);
		System::Void ShowMessenger();
		System::Byte refreshState;

	protected:
		IPAddress ^HostIP;
		String ^HostName;
		NetworkInformation::UnicastIPAddressInformation ^UnicastAddress;
		IPAddress ^SendAddress;
		String ^DomainName;
		String ^UserName = Environment::UserName;
		System::Byte Mode = -1;
		Int16 ListenPort;
		Int16 SendPort;
	private: System::ComponentModel::IContainer^  components;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>

		
#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(launcher::typeid));
			this->Launcher_label_welcome = (gcnew System::Windows::Forms::Label());
			this->statusLabel1 = (gcnew System::Windows::Forms::Label());
			this->statusLabel2 = (gcnew System::Windows::Forms::Label());
			this->progress_status = (gcnew System::Windows::Forms::PictureBox());
			this->launcher_delay = (gcnew System::Windows::Forms::Timer(this->components));
			this->ErrorImage = (gcnew System::Windows::Forms::PictureBox());
			this->errorLabel = (gcnew System::Windows::Forms::Label());
			this->OnErrorButton = (gcnew System::Windows::Forms::Button());
			this->refreshView = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->progress_status))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ErrorImage))->BeginInit();
			this->SuspendLayout();
			// 
			// Launcher_label_welcome
			// 
			this->Launcher_label_welcome->BackColor = System::Drawing::Color::LightSteelBlue;
			this->Launcher_label_welcome->Dock = System::Windows::Forms::DockStyle::Top;
			this->Launcher_label_welcome->Font = (gcnew System::Drawing::Font(L"Segoe UI", 27.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->Launcher_label_welcome->Location = System::Drawing::Point(0, 0);
			this->Launcher_label_welcome->Margin = System::Windows::Forms::Padding(5);
			this->Launcher_label_welcome->Name = L"Launcher_label_welcome";
			this->Launcher_label_welcome->Padding = System::Windows::Forms::Padding(55, 15, 55, 1);
			this->Launcher_label_welcome->Size = System::Drawing::Size(868, 73);
			this->Launcher_label_welcome->TabIndex = 0;
			this->Launcher_label_welcome->Text = L"Uruchamianie aplikacji...";
			// 
			// statusLabel1
			// 
			this->statusLabel1->AutoSize = true;
			this->statusLabel1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 20.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->statusLabel1->Location = System::Drawing::Point(35, 99);
			this->statusLabel1->Name = L"statusLabel1";
			this->statusLabel1->Size = System::Drawing::Size(499, 37);
			this->statusLabel1->TabIndex = 1;
			this->statusLabel1->Text = L"Trwa zbieranie informacji o konfiguracji...";
			// 
			// statusLabel2
			// 
			this->statusLabel2->AutoSize = true;
			this->statusLabel2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->statusLabel2->Location = System::Drawing::Point(90, 149);
			this->statusLabel2->Name = L"statusLabel2";
			this->statusLabel2->Size = System::Drawing::Size(88, 30);
			this->statusLabel2->TabIndex = 2;
			this->statusLabel2->Text = L"Czekaj...";
			// 
			// progress_status
			// 
			this->progress_status->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->progress_status->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"progress_status.Image")));
			this->progress_status->Location = System::Drawing::Point(336, 207);
			this->progress_status->Name = L"progress_status";
			this->progress_status->Size = System::Drawing::Size(220, 20);
			this->progress_status->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->progress_status->TabIndex = 3;
			this->progress_status->TabStop = false;
			this->progress_status->WaitOnLoad = true;
			// 
			// launcher_delay
			// 
			this->launcher_delay->Interval = 3500;
			this->launcher_delay->Tick += gcnew System::EventHandler(this, &launcher::launcher_delay_Tick);
			// 
			// ErrorImage
			// 
			this->ErrorImage->Location = System::Drawing::Point(126, 197);
			this->ErrorImage->Name = L"ErrorImage";
			this->ErrorImage->Size = System::Drawing::Size(34, 40);
			this->ErrorImage->TabIndex = 4;
			this->ErrorImage->TabStop = false;
			this->ErrorImage->Visible = false;
			// 
			// errorLabel
			// 
			this->errorLabel->AutoSize = true;
			this->errorLabel->Font = (gcnew System::Drawing::Font(L"Segoe UI", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->errorLabel->ForeColor = System::Drawing::Color::Red;
			this->errorLabel->Location = System::Drawing::Point(166, 207);
			this->errorLabel->Name = L"errorLabel";
			this->errorLabel->Size = System::Drawing::Size(115, 21);
			this->errorLabel->TabIndex = 5;
			this->errorLabel->Text = L"Error message!";
			this->errorLabel->Visible = false;
			// 
			// OnErrorButton
			// 
			this->OnErrorButton->Location = System::Drawing::Point(768, 230);
			this->OnErrorButton->Name = L"OnErrorButton";
			this->OnErrorButton->Size = System::Drawing::Size(88, 27);
			this->OnErrorButton->TabIndex = 6;
			this->OnErrorButton->Text = L"Zamknij";
			this->OnErrorButton->UseVisualStyleBackColor = true;
			this->OnErrorButton->Visible = false;
			this->OnErrorButton->Click += gcnew System::EventHandler(this, &launcher::OnErrorButton_Click);
			// 
			// refreshView
			// 
			this->refreshView->Interval = 700;
			this->refreshView->Tick += gcnew System::EventHandler(this, &launcher::refreshView_Tick);
			// 
			// launcher
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSize = true;
			this->BackColor = System::Drawing::Color::White;
			this->ClientSize = System::Drawing::Size(868, 269);
			this->Controls->Add(this->OnErrorButton);
			this->Controls->Add(this->errorLabel);
			this->Controls->Add(this->ErrorImage);
			this->Controls->Add(this->progress_status);
			this->Controls->Add(this->statusLabel2);
			this->Controls->Add(this->statusLabel1);
			this->Controls->Add(this->Launcher_label_welcome);
			this->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"launcher";
			this->SizeGripStyle = System::Windows::Forms::SizeGripStyle::Hide;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"launcher";
			this->Load += gcnew System::EventHandler(this, &launcher::launcher_Load);
			this->Shown += gcnew System::EventHandler(this, &launcher::launcher_Shown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->progress_status))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ErrorImage))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void launcher_Shown(System::Object^  sender, System::EventArgs^  e) {
	
	this->launcher_delay->Enabled = true;
}

private: System::Void launcher_Load(System::Object^  sender, System::EventArgs^  e) {
	
}


private: System::Void launcher_delay_Tick(System::Object^  sender, System::EventArgs^  e) {

	this->launcher_delay->Enabled = false;
	
	GetNetworkInformation();
	GetUserInformation();

	ShowMessenger();

}
private: System::Void OnErrorButton_Click(System::Object^  sender, System::EventArgs^  e) {

	Close();
}
private: System::Void refreshView_Tick(System::Object^  sender, System::EventArgs^  e) {
	this->refreshView->Enabled = false;
	this->refreshState = false;
}
};
}
