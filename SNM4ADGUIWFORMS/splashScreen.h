#pragma once

namespace SNM4ADGUIWFORMS {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;
	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class splashScreen : public System::Windows::Forms::Form
	{
	public:
		splashScreen(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~splashScreen()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  ProductNameLabel;
	protected:
	private: System::Windows::Forms::PictureBox^  AppLogo;
	private: System::Windows::Forms::Label^  AuthorLabel;
	private: System::Windows::Forms::Timer^  timer_splash;

	private: System::ComponentModel::IContainer^  components;
	private: System::Windows::Forms::Timer^  splashDelay;

	protected:


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>

		System::Int32 tick_counter;


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(splashScreen::typeid));
			this->ProductNameLabel = (gcnew System::Windows::Forms::Label());
			this->AppLogo = (gcnew System::Windows::Forms::PictureBox());
			this->AuthorLabel = (gcnew System::Windows::Forms::Label());
			this->timer_splash = (gcnew System::Windows::Forms::Timer(this->components));
			this->splashDelay = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->AppLogo))->BeginInit();
			this->SuspendLayout();
			// 
			// ProductNameLabel
			// 
			this->ProductNameLabel->AutoSize = true;
			this->ProductNameLabel->Font = (gcnew System::Drawing::Font(L"Segoe UI", 21.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->ProductNameLabel->Location = System::Drawing::Point(325, 9);
			this->ProductNameLabel->Name = L"ProductNameLabel";
			this->ProductNameLabel->Size = System::Drawing::Size(366, 120);
			this->ProductNameLabel->TabIndex = 0;
			this->ProductNameLabel->Text = L"Simple Network Messenger\r\nfor Active Directory\r\nUsers and Computers";
			this->ProductNameLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// AppLogo
			// 
			this->AppLogo->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"AppLogo.Image")));
			this->AppLogo->Location = System::Drawing::Point(-1, -2);
			this->AppLogo->Name = L"AppLogo";
			this->AppLogo->Size = System::Drawing::Size(225, 211);
			this->AppLogo->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
			this->AppLogo->TabIndex = 1;
			this->AppLogo->TabStop = false;

			// 
			// AuthorLabel
			// 
			this->AuthorLabel->AutoSize = true;
			this->AuthorLabel->Font = (gcnew System::Drawing::Font(L"Segoe UI", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->AuthorLabel->Location = System::Drawing::Point(408, 137);
			this->AuthorLabel->Name = L"AuthorLabel";
			this->AuthorLabel->Size = System::Drawing::Size(199, 30);
			this->AuthorLabel->TabIndex = 2;
			this->AuthorLabel->Text = L"Adrian Kastrau 2016";
			this->AuthorLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			this->AuthorLabel->Visible = false;
			// 
			// timer_splash
			// 
			this->timer_splash->Interval = 50;
			this->timer_splash->Tick += gcnew System::EventHandler(this, &splashScreen::timer1_Tick);
			// 
			// splashDelay
			// 
			this->splashDelay->Interval = 2500;
			this->splashDelay->Tick += gcnew System::EventHandler(this, &splashScreen::splashDelay_Tick);
			// 
			// splashScreen
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::White;
			this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->ClientSize = System::Drawing::Size(827, 205);
			this->Controls->Add(this->AuthorLabel);
			this->Controls->Add(this->AppLogo);
			this->Controls->Add(this->ProductNameLabel);
			this->Cursor = System::Windows::Forms::Cursors::Default;
			this->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"splashScreen";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Simple Network Messenger for Active Directory Users and Computers";
			this->TopMost = true;
			this->Shown += gcnew System::EventHandler(this, &splashScreen::splashScreen_Shown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->AppLogo))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void splashScreen_Shown(System::Object^  sender, System::EventArgs^  e) {
		this->splashDelay->Enabled = true;
		Color myColor = myColor.FromArgb(0, myColor.White);
		this->AuthorLabel->ForeColor = myColor;
		this->AuthorLabel->Visible = true;
	}
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	
	if (this->tick_counter <= 17)
	{
		size_t currentColor = 255 - (15 * tick_counter);
		Color myColor = myColor.FromArgb(currentColor, currentColor, currentColor);
		this->AuthorLabel->ForeColor = myColor;
		tick_counter++;
	}

	else
	{
		this->timer_splash->Enabled = false;
		Thread::Sleep(4000);
		Close();
	}
	

	
}
private: System::Void splashDelay_Tick(System::Object^  sender, System::EventArgs^  e) {

	this->timer_splash->Enabled = true;
	
}
};
}
