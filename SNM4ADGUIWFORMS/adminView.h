#pragma once

#include "windowsHeaders.h"
#include "remoteCommandsDialog.h"

namespace SNM4ADGUIWFORMS {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for adminView
	/// </summary>
	public ref class adminView : public System::Windows::Forms::Form
	{
	public:
		adminView(void)
		{
			this->InitializeComponent();
			//
			//TODO: Add the constructor code here
			//

		
		}
		adminView(launcher ^launcher_data)
		{
			InitializeComponent();

			this->HostName = launcher_data->GetHostName();
			this->HostIP = launcher_data->GetHostIPAddress();
			this->UnicastAddress = launcher_data->GetUnicastAddress();
			this->DomainName = launcher_data->GetDomainName();
			this->UserName = launcher_data->GetUserNameA();
			this->ListenPort = launcher_data->GetListenPort();
			this->SendPort = launcher_data->GetSendPort();
			this->Mode = launcher_data->GetMode();
			this->SendAddress = launcher_data->GetSendAddress();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~adminView()
		{
			if (components)
			{
				delete components;
			}
		}

	protected:
	private: System::Windows::Forms::GroupBox^  MiidleContainer;
	private: System::Windows::Forms::TextBox^  input;
	private: System::Windows::Forms::Button^  sendButton;


	private: System::Windows::Forms::MenuStrip^  menuStrip2;
	private: System::Windows::Forms::ToolStripMenuItem^  plikToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  zamknijToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  pomocToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  oProgramieToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  oAutorzeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  wy�lijPolecenieZdalneToolStripMenuItem;
	private: System::Windows::Forms::StatusStrip^  statusStrip1;
	private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;
	private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel2;



	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
	private: System::ComponentModel::IContainer^  components;

	private:
		IPAddress ^HostIP;
		String ^HostName;
		NetworkInformation::UnicastIPAddressInformation ^UnicastAddress;
		IPAddress ^SendAddress;
		String ^DomainName;
		String ^UserName;
		System::Byte Mode;
		Int16 ListenPort;
		Int16 SendPort;
		Net::Sockets::UdpClient ^UdpClient;
		Thread ^listener = gcnew Thread(gcnew ThreadStart(this, &SNM4ADGUIWFORMS::adminView::UdpListenerStart));
		Thread ^sender;
	private: System::Windows::Forms::ToolTip^  toolTip1;
	private: System::Windows::Forms::ToolStripMenuItem^  zamknijToolStripMenuItem1;
	private: System::Windows::Forms::NotifyIcon^  notifyIcon1;
	private:
		System::Windows::Forms::RichTextBox^  MessageBox;

	public:
		System::Net::Sockets::UdpClient ^GetUdpClient();
		System::Void UdpListenerStart();
		System::Void UpdateMessageLog(System::String ^msg);
		System::Void SendMessage(System::Object ^message);
		System::Void SendMessageCallback();
		System::Void SendRemoteCommand(System::Object ^message);
		System::Void UpdateMessageLogBoldText(System::String ^msg);
		System::Void UpdateMessageLogFromClient(System::String ^msg);

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		 void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(adminView::typeid));
			this->MiidleContainer = (gcnew System::Windows::Forms::GroupBox());
			this->MessageBox = (gcnew System::Windows::Forms::RichTextBox());
			this->input = (gcnew System::Windows::Forms::TextBox());
			this->sendButton = (gcnew System::Windows::Forms::Button());
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->zamknijToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip2 = (gcnew System::Windows::Forms::MenuStrip());
			this->plikToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->zamknijToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->wy�lijPolecenieZdalneToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pomocToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->oProgramieToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->oAutorzeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->toolStripStatusLabel2 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->toolTip1 = (gcnew System::Windows::Forms::ToolTip(this->components));
			this->notifyIcon1 = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->MiidleContainer->SuspendLayout();
			this->contextMenuStrip1->SuspendLayout();
			this->menuStrip2->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// MiidleContainer
			// 
			this->MiidleContainer->Controls->Add(this->MessageBox);
			this->MiidleContainer->Controls->Add(this->input);
			this->MiidleContainer->Controls->Add(this->sendButton);
			this->MiidleContainer->Location = System::Drawing::Point(25, 28);
			this->MiidleContainer->Name = L"MiidleContainer";
			this->MiidleContainer->Size = System::Drawing::Size(522, 335);
			this->MiidleContainer->TabIndex = 1;
			this->MiidleContainer->TabStop = false;
			this->MiidleContainer->Text = L"Witaj!";
			// 
			// MessageBox
			// 
			this->MessageBox->BackColor = System::Drawing::Color::White;
			this->MessageBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->MessageBox->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(238)));
			this->MessageBox->HideSelection = false;
			this->MessageBox->Location = System::Drawing::Point(32, 30);
			this->MessageBox->Name = L"MessageBox";
			this->MessageBox->ReadOnly = true;
			this->MessageBox->Size = System::Drawing::Size(465, 244);
			this->MessageBox->TabIndex = 3;
			this->MessageBox->Text = L"Hej, wygl�da na to, �e wszystko dzia�a! :)\n\nMi�ego korzystania!\n";
			// 
			// input
			// 
			this->input->Location = System::Drawing::Point(32, 293);
			this->input->Name = L"input";
			this->input->Size = System::Drawing::Size(391, 20);
			this->input->TabIndex = 2;
			this->input->Text = L"\r\n";
			this->toolTip1->SetToolTip(this->input, L"Wpisz dowolny tekst i naci�nij klawisz Wy�lij, aby wys�a� wiadomo��\r\n");
			this->input->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &adminView::input_KeyDown);
			// 
			// sendButton
			// 
			this->sendButton->Location = System::Drawing::Point(431, 293);
			this->sendButton->Name = L"sendButton";
			this->sendButton->Size = System::Drawing::Size(75, 23);
			this->sendButton->TabIndex = 1;
			this->sendButton->Text = L"Wy�lij";
			this->sendButton->UseVisualStyleBackColor = true;
			this->sendButton->Click += gcnew System::EventHandler(this, &adminView::sendButton_Click);
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->zamknijToolStripMenuItem1 });
			this->contextMenuStrip1->Name = L"contextMenuStrip1";
			this->contextMenuStrip1->Size = System::Drawing::Size(118, 26);
			this->contextMenuStrip1->Text = L"Zamknij";
			// 
			// zamknijToolStripMenuItem1
			// 
			this->zamknijToolStripMenuItem1->Name = L"zamknijToolStripMenuItem1";
			this->zamknijToolStripMenuItem1->Size = System::Drawing::Size(117, 22);
			this->zamknijToolStripMenuItem1->Text = L"Zamknij";
			this->zamknijToolStripMenuItem1->Click += gcnew System::EventHandler(this, &adminView::zamknijToolStripMenuItem1_Click);
			// 
			// menuStrip2
			// 
			this->menuStrip2->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->plikToolStripMenuItem,
					this->pomocToolStripMenuItem
			});
			this->menuStrip2->Location = System::Drawing::Point(0, 0);
			this->menuStrip2->Name = L"menuStrip2";
			this->menuStrip2->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
			this->menuStrip2->Size = System::Drawing::Size(572, 24);
			this->menuStrip2->TabIndex = 3;
			this->menuStrip2->Text = L"menuStrip2";
			// 
			// plikToolStripMenuItem
			// 
			this->plikToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->zamknijToolStripMenuItem,
					this->wy�lijPolecenieZdalneToolStripMenuItem
			});
			this->plikToolStripMenuItem->Name = L"plikToolStripMenuItem";
			this->plikToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::P));
			this->plikToolStripMenuItem->Size = System::Drawing::Size(38, 20);
			this->plikToolStripMenuItem->Text = L"Plik";
			// 
			// zamknijToolStripMenuItem
			// 
			this->zamknijToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->zamknijToolStripMenuItem->Name = L"zamknijToolStripMenuItem";
			this->zamknijToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Q));
			this->zamknijToolStripMenuItem->Size = System::Drawing::Size(246, 22);
			this->zamknijToolStripMenuItem->Text = L"Zamknij...";
			this->zamknijToolStripMenuItem->Click += gcnew System::EventHandler(this, &adminView::zamknijToolStripMenuItem_Click);
			// 
			// wy�lijPolecenieZdalneToolStripMenuItem
			// 
			this->wy�lijPolecenieZdalneToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->wy�lijPolecenieZdalneToolStripMenuItem->Name = L"wy�lijPolecenieZdalneToolStripMenuItem";
			this->wy�lijPolecenieZdalneToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::R));
			this->wy�lijPolecenieZdalneToolStripMenuItem->Size = System::Drawing::Size(246, 22);
			this->wy�lijPolecenieZdalneToolStripMenuItem->Text = L"Wy�lij polecenie zdalne...";
			this->wy�lijPolecenieZdalneToolStripMenuItem->Click += gcnew System::EventHandler(this, &adminView::wy�lijPolecenieZdalneToolStripMenuItem_Click);
			// 
			// pomocToolStripMenuItem
			// 
			this->pomocToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->pomocToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->oProgramieToolStripMenuItem,
					this->oAutorzeToolStripMenuItem
			});
			this->pomocToolStripMenuItem->Name = L"pomocToolStripMenuItem";
			this->pomocToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::H));
			this->pomocToolStripMenuItem->Size = System::Drawing::Size(57, 20);
			this->pomocToolStripMenuItem->Text = L"Pomoc";
			// 
			// oProgramieToolStripMenuItem
			// 
			this->oProgramieToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->oProgramieToolStripMenuItem->Name = L"oProgramieToolStripMenuItem";
			this->oProgramieToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::P));
			this->oProgramieToolStripMenuItem->Size = System::Drawing::Size(182, 22);
			this->oProgramieToolStripMenuItem->Text = L"O programie";
			this->oProgramieToolStripMenuItem->Click += gcnew System::EventHandler(this, &adminView::oProgramieToolStripMenuItem_Click);
			// 
			// oAutorzeToolStripMenuItem
			// 
			this->oAutorzeToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->oAutorzeToolStripMenuItem->Name = L"oAutorzeToolStripMenuItem";
			this->oAutorzeToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::O));
			this->oAutorzeToolStripMenuItem->Size = System::Drawing::Size(182, 22);
			this->oAutorzeToolStripMenuItem->Text = L"O autorze";
			this->oAutorzeToolStripMenuItem->Click += gcnew System::EventHandler(this, &adminView::oAutorzeToolStripMenuItem_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->toolStripStatusLabel1,
					this->toolStripStatusLabel2
			});
			this->statusStrip1->Location = System::Drawing::Point(0, 372);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(572, 22);
			this->statusStrip1->TabIndex = 4;
			this->statusStrip1->Text = L"Status:";
			// 
			// toolStripStatusLabel1
			// 
			this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
			this->toolStripStatusLabel1->Size = System::Drawing::Size(42, 17);
			this->toolStripStatusLabel1->Text = L"Status:";
			// 
			// toolStripStatusLabel2
			// 
			this->toolStripStatusLabel2->Name = L"toolStripStatusLabel2";
			this->toolStripStatusLabel2->Size = System::Drawing::Size(80, 17);
			this->toolStripStatusLabel2->Text = L"ObecnyStatus";
			// 
			// toolTip1
			// 
			this->toolTip1->IsBalloon = true;
			this->toolTip1->ToolTipIcon = System::Windows::Forms::ToolTipIcon::Info;
			this->toolTip1->ToolTipTitle = L"Wprowadzanie wiadomo�ci";
			// 
			// notifyIcon1
			// 
			this->notifyIcon1->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"notifyIcon1.Icon")));
			this->notifyIcon1->Text = L"SNM4AD";
			this->notifyIcon1->Visible = true;
			// 
			// adminView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(572, 394);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->MiidleContainer);
			this->Controls->Add(this->menuStrip2);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"adminView";
			this->Text = L"Simple Network Messenger for Active Directory Users and Computers - Admininstrato"
				L"r";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &adminView::adminView_FormClosing);
			this->Shown += gcnew System::EventHandler(this, &adminView::adminView_Shown);
			this->MiidleContainer->ResumeLayout(false);
			this->MiidleContainer->PerformLayout();
			this->contextMenuStrip1->ResumeLayout(false);
			this->menuStrip2->ResumeLayout(false);
			this->menuStrip2->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		 }
#pragma endregion
private: System::Void oAutorzeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	about_me about;

	about.ShowDialog();
	}
private: System::Void zamknijToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
private: System::Void oProgramieToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
	MessageBox::Show("Simple Network Messenger" + Environment::NewLine + "Adrian Kastrau"
		+ Environment::NewLine + "Wersja: 1.0", "O programie", MessageBoxButtons::OK, MessageBoxIcon::Information);
}
private: System::Void adminView_Shown(System::Object^  sender, System::EventArgs^  e) {

	this->MiidleContainer->Text = "Witaj, " + this->UserName + "@" + this->DomainName + "!";
	this->toolStripStatusLabel2->Text = "Trwa otwieranie po��cze�...";

	this->listener->IsBackground = true;
	this->listener->Start();
	this->toolStripStatusLabel2->Text = "Gotowy!";
}
private: System::Void sendButton_Click(System::Object^  sender, System::EventArgs^  e) {

	if (this->input->Text == "Wpisz wiadomo��...")
	{
		this->toolStripStatusLabel2->Text = "Usu� domy�ln� wiadomo�� i wprowad� now�";
	}
	else if (this->input->Text->Trim() != "")
	{
		this->sender = gcnew Thread(gcnew ParameterizedThreadStart(this, &SNM4ADGUIWFORMS::adminView::SendMessage));
		this->sender->IsBackground = true;
		this->sender->Start(this->input->Text);
	}

	else
	{
		this->toolStripStatusLabel2->Text = "Wprowadzono pust� wiadomo��!";
	}
}
private: System::Void adminView_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {

}
private: System::Void zamknijToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
	Close();
}
private: System::Void input_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	if (e->KeyCode == Keys::Enter)
	{
		e->SuppressKeyPress = true;
		if (this->input->Text == "Wpisz wiadomo��...")
		{
			this->toolStripStatusLabel2->Text = "Usu� domy�ln� wiadomo�� i wprowad� now�";
		}
		else if (this->input->Text->Trim() != "")
		{
			this->sender = gcnew Thread(gcnew ParameterizedThreadStart(this, &SNM4ADGUIWFORMS::adminView::SendMessage));
			this->sender->IsBackground = true;
			this->sender->Start(this->input->Text);
		}

		else
		{
			this->toolStripStatusLabel2->Text = "Wprowadzono pust� wiadomo��!";
		}
	}
}
private: System::Void wy�lijPolecenieZdalneToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

	remoteCommandsDialog remote;
	remote.ShowDialog();
	System::Int16 ^rpcCall = remote.GetrpcCall();
	short value = safe_cast<short>(rpcCall);

	if (value ==  1)
	{
		this->sender = gcnew Thread(gcnew ParameterizedThreadStart(this, &SNM4ADGUIWFORMS::adminView::SendRemoteCommand));
		this->sender->IsBackground = true;
		this->sender->Start("!:shutdown");
	}

	else if (value == 2)
	{
		this->sender = gcnew Thread(gcnew ParameterizedThreadStart(this, &SNM4ADGUIWFORMS::adminView::SendRemoteCommand));
		this->sender->IsBackground = true;
		this->sender->Start("!:reboot");
	}
	
	else if (value == 3)
	{
		this->sender = gcnew Thread(gcnew ParameterizedThreadStart(this, &SNM4ADGUIWFORMS::adminView::SendRemoteCommand));
		this->sender->IsBackground = true;
		this->sender->Start("!:about_me");
	}
	
}
};
}
