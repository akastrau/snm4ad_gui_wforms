#include "userView.h"


System::Void SNM4ADGUIWFORMS::userView::SendMessage(System::Object ^ message)
{
	IPAddress ^address = this->HostIP;

	auto msg = safe_cast<String^>(message);
	msg = msg->Replace(Environment::NewLine, "");

	String^ formattedMessage = Environment::NewLine + "Wiadomo�� od u�ytkownika " + this->UserName + "@" +
		this->DomainName + ": " + msg;


	Net::Sockets::Socket ^socket = gcnew Net::Sockets::Socket(address->AddressFamily, Sockets::SocketType::Dgram, Sockets::ProtocolType::Udp);

	IPAddress ^broadcast = this->SendAddress;
	System::Text::Encoding ^encoding = System::Text::Encoding::UTF8;
	array<Byte> ^encodedMessage = encoding->GetBytes(formattedMessage);

	IPEndPoint ^endPoint = gcnew IPEndPoint(broadcast, this->SendPort);

	try
	{
		socket->SendTo(encodedMessage, endPoint);
		UpdateMessageLog("Napisa�e� ");
		UpdateMessageLogBoldText("<" + this->UserName + "@" + this->DomainName + ">");
		msg = msg->Replace(Environment::NewLine, "");
		UpdateMessageLog(": " + msg);
		UpdateMessageLog(Environment::NewLine);
		SendMessageCallback();

	}
	catch (Exception ^exception)
	{
		this->toolStripStatusLabel2->Text = exception->Message;
	}

	socket->Close();
}

System::Void SNM4ADGUIWFORMS::userView::SendMessageCallback()
{
	if (InvokeRequired)
	{
		this->Invoke(gcnew Action(this, &SNM4ADGUIWFORMS::userView::SendMessageCallback));
		return;
	}

	this->toolStripStatusLabel2->Text = "Wys�ano pomy�lnie! :)";
	this->input->Text = "";
}

System::Void SNM4ADGUIWFORMS::userView::UpdateMessageLogBoldText(System::String ^ msg)
{
	if (InvokeRequired)
	{
		this->Invoke(gcnew Action<String^>(this, &SNM4ADGUIWFORMS::userView::UpdateMessageLogBoldText), msg);
		return;
	}
	System::Drawing::Font ^currentFont = this->MessageBox->SelectionFont;
	System::Drawing::FontStyle fontStyle = System::Drawing::FontStyle::Bold;
	this->MessageBox->SelectionFont = gcnew System::Drawing::Font(currentFont->FontFamily, currentFont->Size, fontStyle);
	this->MessageBox->AppendText(msg);
	fontStyle = System::Drawing::FontStyle::Regular;
	this->MessageBox->SelectionFont = gcnew System::Drawing::Font(currentFont->FontFamily, currentFont->Size, fontStyle);

}

System::Void SNM4ADGUIWFORMS::userView::UpdateMessageLogFromClient(System::String ^msg)
{
	if (InvokeRequired)
	{
		this->Invoke(gcnew Action<String^>(this, &SNM4ADGUIWFORMS::userView::UpdateMessageLogFromClient), msg);
		return;
	}

	this->MessageBox->AppendText(msg);
	this->MessageBox->SelectionAlignment = HorizontalAlignment::Center;
	this->MessageBox->AppendText(Environment::NewLine);
	this->MessageBox->SelectionAlignment = HorizontalAlignment::Left;
}

System::Void SNM4ADGUIWFORMS::userView::UdpListenerStart()
{
	Net::Sockets::UdpClient ^client;
	IPEndPoint ^incominConnection;

	try
	{
		client = gcnew Net::Sockets::UdpClient(this->ListenPort);

		incominConnection = gcnew IPEndPoint(this->HostIP, 0);
	}
	catch (Exception ^exception)
	{
		MessageBox::Show(exception->Message, "B��d krytyczny!", MessageBoxButtons::OK, MessageBoxIcon::Error);
		this->UpdateMessageLog("UWAGA! Nie mo�na uruchomi� wszystkich funkcji. Port jest zaj�ty!");
		this->toolStripStatusLabel2->Text = "Wyst�pi� b��d :(";;

		return;
	}

	Boolean Done = false;

	while (!Done)
	{
		try {

			array<Byte> ^message = client->Receive(incominConnection);
			System::Text::Encoding ^encoding = System::Text::Encoding::UTF8;
			String ^decodedMessage = encoding->GetString(message);
			/*this->MessageBox->Text += decodedMessage;*/

			if (decodedMessage->Trim() == "")
			{

			}
			else
			{
				this->UpdateMessageLogFromClient(decodedMessage);
				this->toolStripStatusLabel2->Text = "Odebrano wiadomo��";
			}
		}

		catch (Exception ^exception)
		{
			this->toolStripStatusLabel2->Text = exception->Message;
		}

	}

}

System::Void SNM4ADGUIWFORMS::userView::UpdateMessageLog(System::String ^ msg)
{
	if (InvokeRequired)
	{
		this->Invoke(gcnew Action<String^>(this, &SNM4ADGUIWFORMS::userView::UpdateMessageLog), msg);
		return;
	}

	this->MessageBox->AppendText(msg);
}



System::Net::Sockets::UdpClient^ SNM4ADGUIWFORMS::userView::GetUdpClient()
{
	return this->UdpClient;
}
