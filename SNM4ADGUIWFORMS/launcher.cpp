#include "launcher.h"
#include "adminView.h"
#include "userView.h"
using namespace System::Windows::Forms;

System::Void SNM4ADGUIWFORMS::launcher::GetNetworkInformation()
{

	try
	{
		ChangeStatusText("Pobieranie informacji podstawowych o tym komputerze...");
		String ^name = Dns::GetHostName();
		this->HostName = name;
		IPHostEntry ^ip = Dns::GetHostEntry(name);
	}
	catch (Exception ^exception)
	{
		ShowErrorMessage(exception->Message);
	}
	

	String ^_IP = nullptr;

	IPAddress ^HostIP = nullptr;

	try
	{
		IPHostEntry ^_IPHostEntry = System::Net::Dns::GetHostEntry(System::Net::Dns::GetHostName());

		ChangeStatusText("Wyszukiwanie interfejs�w sieciowych...");

		for each (System::Net::IPAddress ^_IPAddress in _IPHostEntry->AddressList)
		{
			if (_IPAddress->AddressFamily.ToString() == "InterNetwork")
			{
				HostIP = _IPAddress;
				this->HostIP = _IPAddress;
				_IP = _IPAddress->ToString();
			}
		}

		array <Net::NetworkInformation::NetworkInterface^> ^adapters = Net::NetworkInformation::NetworkInterface::GetAllNetworkInterfaces();
		IEnumerator^ enumAdapters = adapters->GetEnumerator();

		while (enumAdapters->MoveNext())
		{
			ChangeStatusText("Pobieranie informacji o interfejsach sieciowych...");

			NetworkInformation::NetworkInterface ^adapter = safe_cast<NetworkInformation::NetworkInterface ^>(enumAdapters->Current);
			NetworkInformation::IPInterfaceProperties ^properties = adapter->GetIPProperties();

			NetworkInformation::IPInterfaceProperties ^adapterProperties = adapter->GetIPProperties();
			NetworkInformation::UnicastIPAddressInformationCollection ^uniCast = adapterProperties->UnicastAddresses;

			if (uniCast->Count > 0)
			{
				IEnumerator ^enumUnicastAddress = uniCast->GetEnumerator();
				while (enumUnicastAddress->MoveNext())
				{
					NetworkInformation::UnicastIPAddressInformation ^ uni = safe_cast<NetworkInformation::UnicastIPAddressInformation ^>(enumUnicastAddress->Current);
					if (uni->Address->ToString() == _IP)
					{
						array <Byte> ^complementedMaskBytes = gcnew array<Byte>(4);
						array <Byte> ^broadcastIPBytes = gcnew array<Byte>(4);

						array <Byte> ^uniMaskBytes = uni->IPv4Mask->GetAddressBytes();
						array <Byte> ^hostIPBytes = HostIP->GetAddressBytes();

						for (int i = 0; i < 4; i++)
						{
							complementedMaskBytes[i] = (Byte)~(uniMaskBytes[i]);
							broadcastIPBytes[i] = (Byte)((hostIPBytes[i]) | complementedMaskBytes[i]);
						}

						IPAddress ^ SendAddress = gcnew IPAddress(broadcastIPBytes);
						this->SendAddress = SendAddress;

						ChangeStatusText("Zako�czono wykrywanie interfejs�w sieciowych");
						
						break;
					}

				}
			}
		}
	}
	catch (Exception ^exception)
	{
		ShowErrorMessage(exception->Message);
	}
	
}

System::Void SNM4ADGUIWFORMS::launcher::ChangeStatusText(System::String ^ StatusText)
{
	this->statusLabel2->Visible = false;
	this->statusLabel2->Text = StatusText;
	this->statusLabel2->Visible = true;
	this->Invalidate();

	this->refreshState = true;
	this->refreshView->Enabled = true;

	while (refreshState)
	{
		Application::DoEvents();
	}


}

System::Void SNM4ADGUIWFORMS::launcher::ShowErrorMessage(System::String ^ ErrorMessage)
{
	this->progress_status->Visible = false;

	this->errorLabel->Text = ErrorMessage;
	this->ErrorImage->Visible = true;
	this->errorLabel->Visible = true;
	this->OnErrorButton->Visible = true;
	this->progress_status->Enabled = false;
	this->Refresh();

	this->refreshState = true;
	this->refreshView->Enabled = true;

	while (refreshState)
	{
		Application::DoEvents();
	}

}

System::Void SNM4ADGUIWFORMS::launcher::ShowMessenger()
{
	
	if (this->Mode == 1)
	{
		this->ListenPort = 1222;
		this->SendPort = 1221;
		this->Hide();
		adminView admin(this);

		admin.ShowDialog(this);

		Close();
	}

	else if (this->Mode == 0)
	{
		this->ListenPort = 1221;
		this->SendPort = 1222;
		this->Hide();
		userView ^user = gcnew userView(this);
		user->ShowDialog(this);
		
		Close();
	}

	else
	{
		//ShowErrorMessage("Co� posz�o nie tak przy pr�bie u�ycia bie��cej to�samo�ci. :(");
	}
}

System::Void SNM4ADGUIWFORMS::launcher::GetUserInformation()
{
	
	ChangeStatusText("Trwa pr�ba skontaktowania si� z kontrolerem domeny Active Directory...");

	try
	{
		
		String ^DomainName = ActiveDirectory::Domain::GetComputerDomain()->ToString(); /*"lo1.net"; */
		this->DomainName = DomainName;
		String ^UserName = Environment::UserName + "@" + DomainName;

		ChangeStatusText("Logowanie jako: " + UserName);


		AccountManagement::PrincipalContext ^context = gcnew AccountManagement::PrincipalContext(AccountManagement::ContextType::Domain); 

		AccountManagement::UserPrincipal ^groupList = AccountManagement::UserPrincipal::FindByIdentity(context, Environment::UserName);

		//Invoke(gcnew Action<String^>(this, &SNM4ADGUIWFORMS::launcher::ChangeStatusText), "Trwa pobieranie szczeg�owych informacji o koncie u�ytkownika...");

		//ChangeStatusText("Trwa pobieranie szczeg�owych informacji o koncie u�ytkownika...");

	

		for each (AccountManagement::Principal ^group in  groupList->GetGroups())
		{
			if (group->Name == "Schema Admins" || group->Name == "Enterprise Admins" || group->Name == "Domain Admins")
			{
				//ChangeStatusText("Admin!!!!");
				this->Mode = 1;
				break;
			}

			else if (group->Name == "Domain Users")
			{
				//ChangeStatusText("Uzytkownik");
				this->Mode = 0;
			}

			//this->Mode = 1;

		}

		if (this->Mode == -1)
		{
			ShowErrorMessage("Skontaktuj si� z administratorem systemu!");
			return;
		}


	}
	catch (Exception ^exception)
	{
		ShowErrorMessage(exception->Message);
	}
	
}

System::Net::IPAddress^ SNM4ADGUIWFORMS::launcher::GetHostIPAddress()
{
	return this->HostIP;
}

System::String^ SNM4ADGUIWFORMS::launcher::GetHostName()
{
	return this->HostName;
}

System::Net::NetworkInformation::UnicastIPAddressInformation^ SNM4ADGUIWFORMS::launcher::GetUnicastAddress()
{
	return this->UnicastAddress;
}

System::Net::IPAddress ^ SNM4ADGUIWFORMS::launcher::GetSendAddress()
{
	return this->SendAddress;
}

System::String^ SNM4ADGUIWFORMS::launcher::GetDomainName()
{
	return this->DomainName;
}

System::String ^ SNM4ADGUIWFORMS::launcher::GetUserNameA()
{
	return this->UserName;
}

System::Byte SNM4ADGUIWFORMS::launcher::GetMode()
{
	return this->Mode;
}

System::Int16 SNM4ADGUIWFORMS::launcher::GetListenPort()
{
	return this->ListenPort;
}

System::Int16 SNM4ADGUIWFORMS::launcher::GetSendPort()
{
	return this->SendPort;
}
