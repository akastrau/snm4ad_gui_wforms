#include "splashScreen.h"
#include "launcher.h"


using namespace System;
using namespace System::Windows::Forms;


[STAThread]
int main(array<String^>^ args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	SNM4ADGUIWFORMS::splashScreen form;
	SNM4ADGUIWFORMS::launcher init_messenger;
	Application::Run(%form);
	Application::Run(%init_messenger);
}
